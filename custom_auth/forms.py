from django import forms
from django.contrib.auth.forms import UserCreationForm, PasswordResetForm

from django.template import loader
from custom_auth.models import User

from public.tasks import send_restore_password_email_task


class RegisterForm(UserCreationForm):
    email = forms.EmailField(max_length=200, help_text='Required')

    class Meta:
        model = User
        fields = ('email', 'password1', 'password2')


class PasswordResetFormCustom(PasswordResetForm):
    def send_mail(self, subject_template_name, email_template_name, context, from_email, to_email,
                  html_email_template_name=None):
        subject = loader.render_to_string(subject_template_name, context)

        # Email subject *must not* contain newlines
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string(email_template_name, context)

        html_email = ''
        if html_email_template_name is not None:
            html_email = loader.render_to_string(html_email_template_name, context)

        send_restore_password_email_task.delay(subject, body, from_email, to_email, html_email)
