from django.urls import path, re_path

from custom_auth.views import index, close_registration, RegisterFormView, LoginFormView, LogoutView, activate, \
    PasswordChangeByUser, PasswordChangeDoneViewByUser, PasswordResetViewByUser, PasswordResetDoneViewByUser, \
    PasswordResetConfirmViewByUser, PasswordResetCompleteViewByUser

from .decorators import check_recaptcha

CLOSE_REGISTRATION = True
if not CLOSE_REGISTRATION:
    sign_up = check_recaptcha(RegisterFormView.as_view())
else:
    sign_up = close_registration

urlpatterns = [
    path('', index, name='custom_auth_index'),
    path('sign-up/', sign_up, name='sign_up'),
    path('sign-in/', LoginFormView.as_view(), name='sign_in'),
    path('sign-out/', LogoutView.as_view(), name='sign_out'),
    re_path('activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', activate,
            name='activate'),
    path('password_change/', PasswordChangeByUser.as_view(), name="password_change"),
    path('password_change/done/', PasswordChangeDoneViewByUser.as_view(), name="password_change_done"),
    path('password_reset/', PasswordResetViewByUser.as_view(success_url='done/'), name="password_reset"),
    path('password_reset/done/', PasswordResetDoneViewByUser.as_view(), name="password_reset_done"),

    path('reset/<uidb64>/<token>/', PasswordResetConfirmViewByUser.as_view(success_url='/reset/done/'),
         name="password_reset_confirm"),
    path('reset/done/', PasswordResetCompleteViewByUser.as_view(), name="password_reset_complete"),
]
