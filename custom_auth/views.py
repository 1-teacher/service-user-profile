from django.shortcuts import redirect
from django.views.generic import FormView, RedirectView
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout, get_user
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.auth.views import PasswordChangeView, PasswordChangeDoneView, PasswordResetView, \
    PasswordResetDoneView, PasswordResetConfirmView, PasswordResetCompleteView
from django.utils.decorators import method_decorator
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.shortcuts import render

from custom_auth.forms import RegisterForm, PasswordResetFormCustom
from custom_auth.utils import account_activation_token

from custom_auth.models import User

from public.tasks import send_activate_email_task


def index(request):
    return redirect(reverse_lazy('sign_in'))


def close_registration(request):
    return render(request, 'register_closed.html')


class RegisterFormView(FormView):
    form_class = RegisterForm

    success_url = reverse_lazy("sign_in")

    template_name = "register.html"

    def form_valid(self, form):
        if self.request.recaptcha_is_valid:
            user = form.save(commit=False)
            user.is_active = True
            user.is_activate_account = True
            user.save()

            # to_email = form.cleaned_data.get('email')
            # self._send_activate_email(user, to_email)

            return render(self.request, 'register_success.html', self.get_context_data())

            # return HttpResponse('Для завершения регистрации мы отправили письмо на указанную вами почту, пожалуйста проверьте почту и перейдите по ссылке')
        return render(self.request, 'register.html', self.get_context_data())

    def form_invalid(self, form):
        if form.instance:
            user = User.objects.filter(email=form.instance.email).first()
            if user and not user.is_activate_account:
                user.is_activate_account = True
                user.is_active = True
                user.save()

                form.errors['activate'] = form.error_class([
                    'Теперь вы можете войти в совой аккаунт'
                ])

                # form.errors['activate'] = form.error_class([
                #     'Вы не активировали аккаунт, вам выслано письмо с ключом активации повторно'
                # ])
                # self._send_activate_email(user, form.instance.email)

        return super().form_invalid(form)

    def _send_activate_email(self, user, to_email):
        # current_site = get_current_site(request=self.request)
        from django.conf import settings
        send_activate_email_task.delay({
            'username': user.email,
            'domain': settings.MYSITE,
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'token': account_activation_token.make_token(user),
            'to_email': to_email
        })


class PasswordChangeByUser(PasswordChangeView):
    template_name = 'password_change.html'
    success_url = reverse_lazy('password_change_done')


class PasswordChangeDoneViewByUser(PasswordChangeDoneView):
    template_name = 'password_change_done.html'


class PasswordResetViewByUser(PasswordResetView):
    template_name = 'password_reset_form.html'
    form_class = PasswordResetFormCustom


class PasswordResetDoneViewByUser(PasswordResetDoneView):
    template_name = 'password_reset_done.html'


class PasswordResetConfirmViewByUser(PasswordResetConfirmView):
    template_name = 'password_reset_confirm.html'


class PasswordResetCompleteViewByUser(PasswordResetCompleteView):
    template_name = 'password_reset_complete.html'


class LoginFormView(FormView):
    form_class = AuthenticationForm

    template_name = "login.html"

    success_url = reverse_lazy("account")

    @method_decorator(csrf_protect)
    @method_decorator(never_cache)
    def dispatch(self, *args, **kwargs):
        return super(LoginFormView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):

        login(self.request, form.get_user())
        return super(LoginFormView, self).form_valid(form)

    def set_test_cookie(self):
        self.request.session.set_test_cookie()

    def check_and_delete_test_cookie(self):
        if self.request.session.test_cookie_worked():
            self.request.session.delete_test_cookie()
            return True
        return False

    def get(self, request, *args, **kwargs):
        """
        Same as django.views.generic.edit.ProcessFormView.get(), but adds test cookie stuff
        """
        if get_user(request).is_authenticated:
            return redirect(self.success_url)

        self.set_test_cookie()
        return super(LoginFormView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """
        Same as django.views.generic.edit.ProcessFormView.post(), but adds test cookie stuff
        """
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            self.check_and_delete_test_cookie()
            return self.form_valid(form)
        else:
            self.set_test_cookie()
            return self.form_invalid(form)


class LogoutView(RedirectView):
    """
    Provides users the ability to logout
    """
    url = reverse_lazy('sign_in')

    def get(self, request, *args, **kwargs):
        logout(request)
        return super(LogoutView, self).get(request, *args, **kwargs)


def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.is_activate_account = True
        user.save()
        login(request, user)

        return HttpResponse(
            'Спасибо за подтверждение email. '
            'Сейчас вы можете авторизоваться на сайте. <a href="%s">Войти</a>' % (reverse_lazy('sign_in')))
    else:
        return HttpResponse('Ссылка для активации невалидна! <a href="%s">Войти</a>' % (reverse_lazy('sign_in')))
