# service-user-profile

### Reqiremnetns:

1. python 3.7
2. django 2.3.2
3. django-restframework
4. redis
5. rebbitMQ
6. docker
7. celery

### Description

Service for registration of users participating in the competition for teacher.

### Install

> manage.py makemigrations

> manage.py migrate

> manage.py loaddata fixtures\subject_new.json
> manage.py loaddata fixtures\settings.json
> manage.py loaddata fixtures\clubs.json


### Settings celery and redis for run worker

1. Start redis: 
    > redis-server

2. Start for windows start:  
    > celery worker -A main.celery --loglevel=debug --concurrency=4 --pool=solo


