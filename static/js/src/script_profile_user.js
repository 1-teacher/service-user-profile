$(function () {
    var _main_user_id = $('input[name=user_id]').val();

    var csrftoken = $("input[name=csrfmiddlewaretoken]").val();

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }


    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    var req_settings = function () {
        return $.get('/apiV1/settings/')
    };

    $("#bill_block").hide();
    var is_bill = false;
    var is_upload_file = false;
    req_settings().done(function (data) {
        $.each(data, function (k, o) {
            if (o.parameter === 'bill') {
                if (o.is_active) {
                    $("#bill_block").show();
                } else {
                    $("#bill_block").hide();
                }
                is_bill = o.is_active;
            } else if (o.parameter === 'upload_file_user') {
                is_upload_file = o.is_active;
            }
        })
    });

});