$(function () {
    var _main_user_id = $('input[name=user_id]').val();

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrf_token);
            }
        }
    });

    var setting_user;
    $.get('/apiV1/users/' + _main_user_id + '/', function (data) {
        setting_user = data
    }).done(function () {
        func_upload();
    });

    function func_upload() {
        var is_upload_file = false;
        $.get('/apiV1/settings/', function (data) {
            $.each(data, function (k, o) {
                if (o.parameter === 'upload_file_user') {
                    is_upload_file = o.is_active;
                }
            })
        }).done(function (data) {
            if (is_upload_file) {
                $("#block_file_upload").show();
            } else {
                $("#block_file_upload").hide();
            }

            if (is_upload_file) {
                $.ajax({
                    url: '/apiV1/related_files/?user_id='+page_user_id,
                    type: 'GET',
                    success: function (data) {
                        if (data.src !== undefined) {
                            render_block_uploded_files(data)
                        }
                    },
                });
            }
        });
    }


    function render_block_uploded_files(file) {
        var date = moment(file.modified, 'YYYY-MM-DD HH:mm:ss').format('DD.MM.YYYY HH:mm:ss');
        $("#block_uploded_files pre").html("<a href='" + file.src + "' download='" + file.src + "'>Скачать</a>, обновлен: " + date);
    }


    $('form[name=file_upload]').submit(function (e) {
        e.preventDefault();

        var file = $('#file')[0].files[0];

        var fileSize = file.size / 1048576;
        var fileName = file.name;
        if (fileName.length > 50) {
            console.log("Уменьшите название файла до 50 символов");
            $(".message").append('<div class="alert alert-warning fade show " role="alert">Уменьшите название файла до 50 символов</div>');
            $("#file").replaceWith($("#file").val('').clone(true));
            return false;
        }

        var fileExtensionPattern = /\.(jpg|jpeg|png|pdf|doc|docx|xls|xlsx|rar|zip|7z|ppt|pptx|tif|tiff)$/gmi;
        var isOk = fileName.match(fileExtensionPattern);
        if (!isOk) {
            console.log("Файл должен быть формата: jpg|jpeg|png|pdf|doc|docx|xls|xlsx|rar|zip|7z|ppt|pptx|tif|tiff");
            $(".message").append('<div class="alert alert-warning fade show " role="alert">Файл должен быть формата: jpg,jpeg,png,pdf,doc,docx,xls,xlsx,rar,zip,7z,ppt,pptx,tif,tiff</div>');
            $("#file").replaceWith($("#file").val('').clone(true));
            return false;
        }
        if (fileSize > 15) {
            console.log("Файл должен быть меньше 15МБ.");
            $(".message").append('<div class="alert alert-warning fade show " role="alert">Файл должен быть меньше 15МБ</div>');
            $("#file").replaceWith($("#file").val('').clone(true));
            return false;
        }


        var fileData = new FormData();
        fileData.append('src', file);

        let req = $.ajax({
            url: '/apiV1/related_files/?user_id='+page_user_id,
            type: 'GET'
        });

        req.then(function (data) {
                if (data.src === undefined) {
                    console.log();
                    $.ajax({
                        url: '/apiV1/related_files/?user_id='+page_user_id,
                        type: 'POST',
                        data: fileData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            render_block_uploded_files(data);
                            $(".message").html('<div class="alert alert-success fade show " role="alert">Файл загрузился</div>');
                            $("#file").replaceWith($("#file").val('').clone(true));
                        },
                    });
                } else {
                    $.ajax({
                        url: '/apiV1/related_files/?user_id='+page_user_id,
                        type: 'PUT',
                        data: fileData,
                        cache: false,
                        contentType: false,
                        processData: false,
                        success: function (data) {
                            render_block_uploded_files(data);
                            $(".message").html('<div class="alert alert-success fade show " role="alert">Файл обновился</div>');
                            $("#file").replaceWith($("#file").val('').clone(true));
                        },
                    });
                }
            }, function (err) {
                $(".message").html('<div class="alert alert-success fade show " role="alert">Ошибка при загрузке файла</div>');
            }
        ).done(function () {
            console.log('Последовательность выполнена');
        });


    });
});