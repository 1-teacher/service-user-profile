$(function () {
    var _main_user_id = $('input[name=user_id]').val();


    var csrftoken = $("input[name=csrfmiddlewaretoken]").val();

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    var is_bill = false;
    var is_upload_file = false;

    var req_settings = function () {
        return $.get('/apiV1/settings/')
    };
    req_settings().done(function (data) {
        $.each(data, function (k, o) {
            if (o.parameter === 'bill') {
                is_bill = o.is_active;
            } else if (o.parameter === 'upload_file_user') {
                is_upload_file = o.is_active;
            }
        })
    });

    var req_user = function () {
        return $.get('/apiV1/users/' + _main_user_id + '/')
    };

    function render_profile(profile) {
        var html = '<table>';

        html += "<tr><td>Фамилия</td><td>" + profile.last_name + "</td></tr>";
        html += "<tr><td>Имя</td><td>" + profile.first_name + "</td></tr>";
        html += "<tr><td>Отчество</td><td>" + profile.patronymic + "</td></tr>";
        html += "<tr><td>Город</td><td>" + profile.city + "</td></tr>";
        html += "<tr><td>Место работы</td><td>" + profile.place_work + "</td></tr>";
        html += "<tr><td>Должность</td><td>" + profile.employee_position + "</td></tr>";
        html += "<tr><td>Звание</td><td>" + profile.rank + "</td></tr>";
        html += "<tr><td>Раб. телефон</td><td>" + profile.phone_work + "</td></tr>";
        html += "<tr><td>Моб. телефон</td><td>" + profile.phone_mobile + "</td></tr>";
        html += "<tr><td>Email</td><td>" + profile.email + "</td></tr>";
        html += "<tr><td>VK</td><td>" + profile.link_vk + "</td></tr>";
        html += "<tr><td>Instagram</td><td>" + profile.link_instagram + "</td></tr>";
        html += "<tr><td>Facebook</td><td>" + profile.link_facebook + "</td></tr>";
        html += "<tr><td>Одноклассники</td><td>" + profile.link_ok + "</td></tr>";
        if (profile.club !== null) {
            html += "<tr><td>Секция</td><td>" + profile.club.name + "</td></tr>";
        }
        if (is_upload_file) {
            if (profile.file !== null) {
                html += "<tr><td>Файл</td><td> <a href='" + profile.file.src + "' download='" + profile.file.src + "'>Скачать</a></td></tr>";
            } else {
                html += "<tr><td>Файл</td><td> - </td></tr>";
            }
        }
        if (profile.region_code !== null) {
            html += "<tr><td>Регион</td><td>" + profile.region_code.name + "</td></tr>";
        }

        html += "</table>";
        $(".personal_info pre").html(html);
    }

    function render_bill(bill) {
        if (bill !== null) {
            var bill_html = '<h3>Cчет</h3><table>';

            bill_html += "<tr><td>Номер счета: </td><td>" + bill.bill_code + "</td></tr>";
            bill_html += "<tr><td>Дата создания счета: </td><td>" + bill.created + "</td></tr>";

            bill_html += "<tr><td>Сумма: </td><td>" + bill.price + "</td></tr>";

            bill_html += "<tr><td>Оплата: </td><td>" + (bill.is_paid ? 'готово' : 'не оплачено') + "</td></tr>";

            bill_html += "</table>";

            $(".personal_bill pre").html(bill_html);
        }
    }

    function check_bill_input(is_paid) {
        if (is_paid) {
            $("#bill_paid").find("input[name=is_paid]").prop("checked", true);
        } else {
            $("#bill_paid").find("input[name=is_paid]").prop("checked", false);
        }
    }


    function req_change_staff(is_staff) {
        if (_main_user_id !== null || _main_user_id !== undefined) {
            return $.ajax({
                url: '/apiV1/users/' + _main_user_id + '/change_staff/',
                type: 'put',
                data: JSON.stringify({is_staff: is_staff}),
                contentType: "application/json",
                dataType: 'json',
                success: function (data) {
                    $(".message").html('<div class="alert alert-success fade show " role="alert">Права успешно изменились</div>');
                    if (data && data.is_staff) {
                        $("#enable_staff").hide();
                        $("#disable_staff").show();

                        $(".personal_info").hide();
                        $("#bill_block").hide();
                    }
                    if (data && !data.is_staff) {
                        $("#disable_staff").hide();
                        $("#enable_staff").show();

                        $(".personal_info").show();
                        req_settings().done(function (data) {
                            $.each(data, function (k, o) {
                                if (o.parameter === 'bill') {
                                    if (o.is_active) {
                                        $("#bill_block").show();
                                    } else {
                                        $("#bill_block").hide();
                                    }

                                    return false;
                                }
                            })
                        });

                        req_user().done(function (data) {
                            render_profile(Object.assign({}, data.profile, {'email': data.email, 'file': data.file}));
                            render_bill(data.bill);
                            if (data.bill) {
                                check_bill_input(data.bill.is_paid);
                            }
                        });

                    }
                },
                error: function (request, msg, error) {
                    console.log(error);
                    $(".message").html('<div class="alert alert-danger fade show " role="alert">Произошла ошибка при изменении прав пользователя ' + error + '</div>');
                }
            });
        }

        return null;
    }

    $("#enable_staff").click(function () {
        req_change_staff(true);
    });

    $("#disable_staff").click(function () {
        req_change_staff(false);
    });


    var init = function () {
        req_settings().done(function (data) {
            $.each(data, function (k, o) {
                if (o.parameter === 'bill') {
                    is_bill = o.is_active;
                } else if (o.parameter === 'upload_file_user') {
                    is_upload_file = o.is_active;
                }
            });
            req_user().done(function (data) {
                render_profile(Object.assign({}, data.profile, {'email': data.email, 'file': data.file}));

                render_bill(data.bill);
                if (data.bill) {
                    check_bill_input(data.bill.is_paid);
                }
            });
        });


        if (_main_user_id !== null || _main_user_id !== undefined) {

            var is_staff_user = false;

            $.ajax({
                url: '/apiV1/users/' + _main_user_id + '/change_staff/',
                type: 'get',
                success: function (data) {
                    is_staff_user = data.is_staff;
                    if (is_staff_user) {
                        $("#enable_staff").hide();
                        $("#disable_staff").show();
                    } else {
                        $("#disable_staff").hide();
                        $("#enable_staff").show();
                    }
                },
                error: function (request, msg, error) {
                    console.log(error);
                    // $(".message").append(
                    //     '<div class="alert alert-danger fade show " role="alert">Произошла ошибка при зашрузке данных ' + error + '</div>'
                    // );
                }
            });


            if (is_bill) {

                $.ajax({
                    url: '/apiV1/users/' + _main_user_id + '/bills/',
                    type: 'get',
                    success: function (data) {
                        var _bill = $("#bill");
                        _bill.find("input[name=price]").val(data.price);
                        _bill.find("input[name=bill_code]").val(data.bill_code);

                        var _bill_paid = $("#bill_paid");
                        _bill_paid.find("input[name=is_paid]").prop("checked", data.is_paid);
                    },
                    error: function (request, msg, error) {
                        console.log(error);
                        // $(".message").append(
                        //     '<div class="alert alert-danger fade show " role="alert">Произошла ошибка при зашрузке данных ' + error + '</div>'
                        // );
                    }
                });

                $(".personal_info").show();
                $("#bill_block").hide();

            } else {
                $(".personal_info").show();
                $("#bill_block").show();
            }
        }

    };

    if (_main_user_id !== undefined) {
        init();
    }

    $("#bill button").click(function (e) {
        e.preventDefault();

        var _bill = $("#bill");
        var data = {
            user: _main_user_id,
            is_paid: false,
            price: _bill.find("input[name=price]").val(),
            bill_code: _bill.find("input[name=bill_code]").val()
        };

        //1. проверить заведен ли уже счет, если да то изменить , если нет то создать новый
        if (_main_user_id !== null || _main_user_id !== undefined) {
            $.ajax({
                url: '/apiV1/users/' + _main_user_id + '/bills/',
                type: 'get',
                success: function (result) {
                    $.ajax({
                        url: '/apiV1/users/' + _main_user_id + '/bills/',
                        type: 'PUT',
                        data: JSON.stringify({
                            bill_code: data.bill_code,
                            id: result.id,
                            is_paid: data.is_paid,
                            price: data.price,
                            user: data.user
                        }),
                        contentType: "application/json",
                        dataType: 'json',
                        success: function (result) {
                            console.log(result);
                            $(".message").html(
                                '<div class="alert alert-success fade show " role="alert">Обновлено</div>'
                            );

                            render_bill(result);
                            check_bill_input(result.is_paid);
                        },
                        error: function (request, msg, error) {
                            console.log(error);
                            $(".message").html('<div class="alert alert-danger fade show " role="alert">Произошла ошибка при обновлении счета ' + error + '</div>');
                        }
                    });
                },
                error: function (xhr, msg, error) {
                    if (xhr.status == 404) {
                        $.ajax({
                            url: '/apiV1/users/' + _main_user_id + '/bills/',
                            type: 'POST',
                            data: JSON.stringify(data),
                            contentType: "application/json",
                            dataType: 'json',
                            success: function (result) {
                                console.log(result);
                                $.ajax({
                                    url: '/apiV1/users/' + _main_user_id + '/bills/',
                                    type: 'get',
                                    success: function (result) {
                                        $(".message").html(
                                            '<div class="alert alert-success fade show " role="alert">Счет создан успешно!</div>'
                                        );

                                        render_bill(result);
                                        check_bill_input(result.is_paid);
                                    }
                                });
                            },
                            error: function (request, msg, error) {
                                console.log(error);
                                $(".message").html(
                                    '<div class="alert alert-danger fade show " role="alert">Произошла ошибка при создании счета ' + error + '</div>'
                                );
                            }
                        });
                    }
                    console.log(error);
                }
            });
        }

    });

    $("#bill_paid button").click(function (e) {
        e.preventDefault();
        var _bill_paid = $("#bill_paid");
        var data = {
            is_paid: _bill_paid.find("input[name=is_paid]").prop("checked")
        };

        if (_main_user_id !== null || _main_user_id !== undefined) {

            $.ajax({
                url: '/apiV1/users/' + _main_user_id + '/bills/',
                type: 'get',
                success: function (result) {
                    $.ajax({
                        url: '/apiV1/bills/' + result.id + '/',
                        type: 'PUT',
                        data: JSON.stringify({
                            id: result.id,
                            is_paid: data.is_paid
                        }),
                        contentType: "application/json",
                        dataType: 'json',
                        success: function (result) {
                            console.log(result);
                            $.ajax({
                                url: '/apiV1/users/' + _main_user_id + '/bills/',
                                type: 'get',
                                success: function (result) {
                                    $(".message").html(
                                        '<div class="alert alert-success fade show " role="alert">Статус оплаты изменен успешно!</div>'
                                    );

                                    render_bill(result);
                                    check_bill_input(result.is_paid);
                                }
                            });
                        },
                        error: function (request, msg, error) {
                            console.log(error);
                            $(".message").html(
                                '<div class="alert alert-danger fade show " role="alert">Произошла ошибка при при изменении статуса оплаты ' + error + '</div>'
                            );
                        }
                    });
                },
                error: function (xhr, msg, error) {
                }
            });

        }

    });

});