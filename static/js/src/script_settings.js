$(function () {
    var csrftoken = $("input[name=csrfmiddlewaretoken]").val();

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    $.ajax({
        url: '/apiV1/settings/',
        type: 'get',
        data: JSON.stringify({is_active: $(this).is(':checked')}),
        contentType: "application/json",
        dataType: 'json',
        success: function (data) {
            $.each(data, function (k, o) {
                if (o.parameter === 'bill') {
                    $("#is_show_bill").prop('checked', o.is_active);
                } else if (o.parameter === 'upload_file_user') {
                    $("#is_upload_file").prop('checked', o.is_active);
                }
            })
        },
        error: function (request, msg, error) {
            console.log(error);
            $(".message").append('<div class="alert alert-danger alert-dismissible fade show " role="alert">Произошла ошибка при загрузке настроек ' + error +
                '  <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '    <span aria-hidden="true">&times;</span>' +
                '  </button>' + '</div>');
        }
    });

    $("#is_show_bill").change(function () {
        $.ajax({
            url: '/apiV1/settings/bill/',
            type: 'put',
            data: JSON.stringify({is_active: $(this).is(':checked')}),
            contentType: "application/json",
            dataType: 'json',
            success: function (data) {
                $(".message").append('<div class="alert alert-success alert-dismissible fade show " role="alert">Настройки успешно изменены' +
                    '  <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '    <span aria-hidden="true">&times;</span>' +
                    '  </button>' + '</div>');
            },
            error: function (request, msg, error) {
                console.log(error);
                $(".message").append('<div class="alert alert-danger alert-dismissible fade show " role="alert">Произошла ошибка при изменении настройки ' + error +
                    '  <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '    <span aria-hidden="true">&times;</span>' +
                    '  </button>' + '</div>');
            }
        });
    });

    $("#is_upload_file").change(function () {
        $.ajax({
            url: '/apiV1/settings/upload_file_user/',
            type: 'put',
            data: JSON.stringify({parameter: "upload_file_user", is_active: $(this).is(':checked')}),
            contentType: "application/json",
            dataType: 'json',
            success: function (data) {
                $(".message").append('<div class="alert alert-success alert-dismissible fade show " role="alert">Настройки успешно изменены' +
                    '  <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '    <span aria-hidden="true">&times;</span>' +
                    '  </button>' + '</div>');
            },
            error: function (request, msg, error) {
                console.log(error);
                $(".message").append('<div class="alert alert-danger alert-dismissible fade show " role="alert">Произошла ошибка при изменении настройки ' + error +
                    '  <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                    '    <span aria-hidden="true">&times;</span>' +
                    '  </button>' +
                    '</div>');
            }
        });
    });


});