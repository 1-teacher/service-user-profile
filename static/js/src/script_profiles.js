$(function () {
    var _main_user_id = $('input[name=user_id]').val();

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    var is_bill = false;
    var is_upload_file = false;

    var req_settings = function () {
        return $.get('/apiV1/settings/')
    };
    req_settings().done(function (data) {
        $.each(data, function (k, o) {
            if (o.parameter === 'bill') {
                is_bill = o.is_active;
            } else if (o.parameter === 'upload_file_user') {
                is_upload_file = o.is_active;
            }
        })
    });


    function widget_name(id, name, email) {
        return '<div class="widget-content p-0">' +
            '<div class="widget-content-wrapper">' +
            // '<div class="widget-content-left mr-3">' +
            // '<div class="widget-content-left">' +
            // '<a href="/account/profiles/' + id + '/">' +
            // '<img width="40" class="rounded-circle" src="/static/assets/images/avatars/4.jpg" alt="">' +
            // '</a>' +
            // '</div>' +
            // '</div>' +
            '<div class="widget-content-left flex2">' +
            '<div class="widget-heading"><a href="/account/profiles/' + id + '/">' + name + '</a></div>' +
            '<div class="widget-subheading opacity-7"><a href="/account/profiles/' + id + '/">' + email + '</a></div>' +
            '</div>' +
            '</div>' +
            '</div>';
    }

    function render_table_user(users) {
        var html = '<table class="align-middle mb-0 table table-borderless table-striped table-hover">';
        html += '<thead><tr>';
        html += '<th class="text-center">#</th>';
        html += '<th>фио</th>';
        html += '<th>номер мобильного</th>';
        html += '<th>субъект</th>';
        if (is_upload_file) {
            html += '<th>прикреплён ли файл</th>';
        }
        if (is_bill) {
            html += '<th>номер счета</th>';
            html += '<th>сумма</th>';
            html += '<th>оплата</th>';
        }

        html += '</tr></thead>';
        if (users.length === 0) {
            html += '<tr><td></td><td>не найдено</td><td></td><td></td><td></td><td></td><td></td><td></td></tr>'
        }
        $.each(users, function (k, v) {
            html += '<tr>';
            html += '<td class="text-center text-muted"></td>';
            var _name = v.profile.last_name + ' ' + v.profile.first_name + ' ' +  v.profile.patronymic;
            if (_name !== '') {
                html += '<td>' + widget_name(v.id, _name, v.email) + '</td>';
            } else {
                html += '<td>' + widget_name(v.id, 'не указано', v.email) + '</td>';
            }
            html += '<td>' + v.profile.phone_mobile + '</td>';
            if (v.profile.region_code) {
                html += '<td>' + v.profile.region_code.name + '</td>';
            } else {
                html += '<td>-</td>';
            }
            if (is_upload_file) {
                if (v.file) {
                    html += '<td>Прикреплен</td>';
                } else {
                    html += '<td>-</td>';
                }
            }

            if (is_bill) {
                if (v.bill) {
                    html += '<td>' + v.bill.bill_code + '</td>';
                    html += '<td>' + v.bill.price + '</td>';
                    html += '<td>' + (v.bill.is_paid ? '<div class="badge badge-success">оплачено</div>' : '<div class="badge badge-warning">не оплачено</div>') + '</td>';
                } else {
                    html += '<td>-</td>';
                    html += '<td>-</td>';
                    html += '<td>-</td>';
                }
            }
            html += '</tr>';
        });
        html += '</table>';


        $(".wrap_profiles_table").html(html);
    }

    var url = '/apiV1/users/?subject_id=&is_paid=&sort_by=&is_staff=&club_id=&';

    paging(url);

    function paging(_url) {
        $('#paging_profiles').pagination({
            dataSource: _url,
            alias: {
                pageNumber: 'page',
                pageSize: 'page_size'
            },
            showNavigator: true,
            formatNavigator: '<span style="float:left">Всего <%= totalNumber %> пользователей</span>',
            locator: 'results',
            pageNumber: 1,
            pageSize: 100,
            totalNumberLocator: function (response) {
                return response.count
            },
            ajax: {
                beforeSend: function () {
                    $(".wrap_profiles_table").html('Загрузка ...');
                }
            },
            ulClassName: 'pagination',
            callback: function (data, pagination) {
                render_table_user(data);
            }
        });
    }

    $('#search').on('input', function () {
        var needle = $(this).val();
        if (needle !== '') {
            var _url_search = '/apiV1/users/?q=' + needle + '&';
            paging(_url_search);
        } else {
            render_profiles();
        }
    });

    function render_profiles() {
        var is_paid = $("#is_paid").is(':checked') ? 'y' : 'n';
        var is_staff = $("#is_staff").is(':checked') ? 'y' : 'n';
        var subject_id = $("select#filter_subject option:checked").val();
        var club_id = $("select#filter_club option:checked").val();

        var sort = '';
        var sort_val = $("select#sort_fio_reg option:checked").val();
        if (sort_val === 'fio_asc') {
            sort = 'fio';
        } else if (sort_val === 'fio_desc') {
            sort = '-fio';
        } else if (sort_val === 'region_asc') {
            sort = 'subject';
        } else if (sort_val === 'region_desc') {
            sort = '-subject';
        }

        var url_temp = '/apiV1/users/?subject_id=' + subject_id + '&is_paid=' + is_paid + '&sort_by=' + sort + '&is_staff=' + is_staff + '&club_id='+ club_id+'&';

        paging(url_temp);
    }


    $("#is_paid, #is_staff, select#filter_subject, select#filter_club, select#sort_fio_reg").change(function () {
        render_profiles();
    });

});