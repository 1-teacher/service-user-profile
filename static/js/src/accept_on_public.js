$(function () {
    var _main_user_id = $('input[name=user_id]').val();

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrf_token);
            }
        }
    });

    $.ajax({
        url: '/apiV1/accept-on-public/' + _main_user_id + '/',
        type: 'get',
        data: JSON.stringify({is_accept_on_public: $(this).is(':checked')}),
        contentType: "application/json",
        dataType: 'json',
        success: function (data) {
            $("#is_accept_on_public").prop('checked', data.is_accept_on_public);
        },
        error: function (request, msg, error) {
            $(".message").append('<div class="alert alert-danger  alert-dismissible fade show " role="alert">Произошла ошибка при загрузке ' + error +
                '  <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '    <span aria-hidden="true">&times;</span>' +
                '  </button>' + '</div>');
        }
    });

    $("#is_accept_on_public").change(function () {
        $.ajax({
            url: '/apiV1/accept-on-public/' + _main_user_id + '/',
            type: 'put',
            data: JSON.stringify({is_accept_on_public: $(this).is(':checked')}),
            contentType: "application/json",
            dataType: 'json',
            success: function (data) {
                $("#is_accept_on_public").prop('checked', data.is_accept_on_public);
                $(".message").append('<div class="alert alert-success alert-dismissible fade show " role="alert">Согласие на публикацию статей в сборнике Конференции изменено'+
                    '  <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '    <span aria-hidden="true">&times;</span>' +
                '  </button>'+'</div>');
            },
            error: function (request, msg, error) {
                $(".message").append('<div class="alert alert-danger alert-dismissible fade show " role="alert">Произошла ошибка при загрузке ' + error +
                    '  <button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '    <span aria-hidden="true">&times;</span>' +
                '  </button>'+'</div>');
            }
        });
    });

});