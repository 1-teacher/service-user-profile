from rest_framework import serializers
from public.models import Profile, SubjectRegion, Bill, File, Setting, Club
from custom_auth.models import User


class SubjectRegionSerializers(serializers.ModelSerializer):
    class Meta:
        model = SubjectRegion
        fields = ['id', 'name', 'region_code']


class ClubSerializers(serializers.ModelSerializer):
    class Meta:
        model = Club
        fields = ['id', 'name']


class ProfileSerializers(serializers.ModelSerializer):
    region_code = SubjectRegionSerializers(read_only=True)
    club = ClubSerializers(read_only=True)

    class Meta:
        model = Profile
        fields = [
            'first_name', 'last_name', 'patronymic', 'city', 'place_work',
            'employee_position', 'phone_work', 'phone_mobile',
            'rank', 'club',
            'link_vk', 'link_instagram', 'link_facebook', 'link_ok',
            'region_code'
        ]


class BillSerializers(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    bill_code = serializers.CharField()
    price = serializers.CharField()
    is_paid = serializers.BooleanField()
    created = serializers.DateTimeField(read_only=True)
    modified = serializers.DateTimeField(read_only=True)

    def update(self, instance, validated_data):
        instance.user = validated_data.get('user', instance.user)
        instance.bill_code = validated_data.get('bill_code', instance.bill_code)
        instance.price = validated_data.get('price', instance.price)
        instance.is_paid = validated_data.get('is_paid', instance.is_paid)
        instance.save()

        return instance

    class Meta:
        model = Bill
        fields = ['id', 'user', 'bill_code', 'price', 'is_paid', 'created', 'modified']


class BillIsPaidSerializers(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    is_paid = serializers.BooleanField()
    created = serializers.DateTimeField(read_only=True)
    modified = serializers.DateTimeField(read_only=True)

    def update(self, instance, validated_data):
        instance.is_paid = validated_data.get('is_paid', instance.is_paid)
        instance.save()

        return instance

    class Meta:
        model = Bill
        fields = ['id', 'is_paid', 'created', 'modified']


class FileSerializers(serializers.ModelSerializer):
    class Meta:
        model = File
        fields = ['src', 'created', 'modified']


class UserSerializers(serializers.ModelSerializer):
    profile = ProfileSerializers(read_only=True)
    bill = BillSerializers(read_only=True)
    file = FileSerializers(read_only=True)

    class Meta:
        model = User
        fields = [
            'id', 'email', 'is_staff', 'is_active', 'is_activate_account', 'date_joined', 'profile',
            'bill', 'file'
        ]


class UserChangeStaffSerializers(serializers.ModelSerializer):
    id = serializers.IntegerField(read_only=True)
    is_staff = serializers.BooleanField()

    def update(self, instance, validated_data):
        instance.is_staff = validated_data.get('is_staff', instance.is_staff)
        instance.is_active = True
        instance.save()

        return instance

    class Meta:
        model = User
        fields = ['id', 'is_staff']


class UserAcceptPublicSerializers(serializers.ModelSerializer):
    user_id = serializers.IntegerField(read_only=True)
    is_accept_on_public = serializers.BooleanField()

    def update(self, instance, validated_data):
        instance.is_accept_on_public = validated_data.get('is_accept_on_public', instance.is_accept_on_public)
        instance.save()

        return instance

    class Meta:
        model = Profile
        fields = ['user_id', 'is_accept_on_public']


class SettingSerializers(serializers.ModelSerializer):
    parameter = serializers.CharField(read_only=True)
    is_active = serializers.BooleanField()

    def update(self, instance, validated_data):
        return super().update(instance, validated_data)

    class Meta:
        model = Setting
        fields = ['id', 'parameter', 'is_active']
