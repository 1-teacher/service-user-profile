from rest_framework import viewsets, views
from rest_framework import permissions
from rest_framework import generics
from rest_framework.response import Response

from django.db.models import F
from django.db.models.functions import Concat
from custom_auth.models import User
from public.models import Profile, Bill, Setting, File
from django.shortcuts import get_object_or_404
from . import serializers

from rest_framework.pagination import LimitOffsetPagination, PageNumberPagination


class StandardPageNumberPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 1000


class StandardLimitOffsetPagination(LimitOffsetPagination):
    default_limit = 10
    max_limit = 1000


class OwnProfilePermission(permissions.BasePermission):
    """
    Object-level permission to only allow updating his own profile
    """

    def has_object_permission(self, request, view, obj):
        # Read permissions are allowed to any request,
        # so we'll always allow GET, HEAD or OPTIONS requests.
        if request.method in permissions.SAFE_METHODS:
            return obj.id == request.user.id or request.user.is_staff

        return obj.user == request.user or request.user.is_staff


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializers
    pagination_class = StandardPageNumberPagination
    permission_classes = [permissions.IsAuthenticated, OwnProfilePermission]

    def get_queryset(self):

        queryset = self.queryset.filter()

        # filter ?subject_id=12 , ? is_paid=y or is_paid=n, ?is_staff=y or is_staff=n
        subject_id = self.request.query_params.get('subject_id', None)
        club_id = self.request.query_params.get('club_id', None)
        is_paid = self.request.query_params.get('is_paid', None)
        is_staff = self.request.query_params.get('is_staff', None)

        # order (asc| desc) ?sort_by=-fio, ?sort_by=subject

        sort_by = self.request.query_params.get('sort_by', None)
        order = ''
        field = ''
        if sort_by:
            order = '-' if sort_by and sort_by[0] == "-" else ''
            field = sort_by[1:] if order == '-' else sort_by

        # search by fio ?q=test
        search_query = self.request.query_params.get('q', None)

        # filter
        if subject_id not in (None, 0, "0", ''):
            user_ids = Profile.objects.filter(region_code_id=subject_id).values_list('user_id', flat=True)
            queryset = queryset.filter(id__in=user_ids)
        if club_id not in (None, 0, "0", ''):
            user_ids = Profile.objects.filter(club_id=club_id).values_list('user_id', flat=True)
            queryset = queryset.filter(id__in=user_ids)

        if is_paid == "y":
            user_ids = Bill.objects.filter(is_paid=True).values_list('user_id', flat=True)
            queryset = queryset.filter(id__in=user_ids)

        if is_staff == "y":
            queryset = queryset.filter(is_staff=True, is_superuser=False)
        else:
            queryset = queryset.filter(is_superuser=False)

        # order
        if field == "subject":
            order_field = order + 'profile__region_code__name'
            queryset = queryset.order_by(order_field)
        elif field == "fio":
            order_field = order + 'fio'
            queryset = queryset.annotate(
                fio=Concat(F('profile__last_name'), F('profile__first_name'), F('profile__patronymic'))
            ).order_by(order_field)

        # search
        if search_query:
            queryset = queryset.annotate(
                fio=Concat(F('profile__last_name'), F('profile__first_name'), F('profile__patronymic'))
            ).filter(fio__icontains=search_query)

        return queryset


class UserViewSetDetail(viewsets.ReadOnlyModelViewSet):
    queryset = User.objects.all()
    serializer_class = serializers.UserSerializers
    pagination_class = StandardPageNumberPagination
    permission_classes = [permissions.IsAuthenticated, OwnProfilePermission]


class BillView(generics.ListCreateAPIView, generics.RetrieveUpdateAPIView):
    serializer_class = serializers.BillSerializers
    permission_classes = [permissions.IsAdminUser]
    queryset = Bill.objects.all()

    def get(self, request, *args, **kwargs):
        if 'user_id' in kwargs:
            queryset = self.get_queryset().filter(user_id=self.kwargs['user_id'])
            instance = get_object_or_404(queryset, user_id=self.kwargs['user_id'])
            serializer = self.get_serializer(instance)
            return Response(serializer.data)

        return super().get(request, *args, **kwargs)

    def get_object(self):
        queryset = self.get_queryset().filter(user_id=self.kwargs['user_id'])
        obj = get_object_or_404(queryset, user_id=self.kwargs['user_id'])
        return obj

    def put(self, request, *args, **kwargs):
        return super().put(request, *args, **kwargs)


class BillIsPaidView(generics.RetrieveUpdateAPIView):
    serializer_class = serializers.BillIsPaidSerializers
    permission_classes = [permissions.IsAdminUser]
    queryset = Bill.objects.all()

    def get_object(self):
        pk = self.kwargs["pk"]
        return get_object_or_404(Bill, pk=pk)


class UserChangeStaffView(generics.RetrieveUpdateAPIView):
    serializer_class = serializers.UserChangeStaffSerializers
    permission_classes = [permissions.IsAdminUser]
    http_method_names = ['get', 'put']
    queryset = User.objects.all()

    def get_object(self):
        queryset = self.get_queryset().filter(id=self.kwargs['user_id'])
        obj = get_object_or_404(queryset, id=self.kwargs['user_id'])
        return obj


class UserAcceptPublicView(generics.RetrieveUpdateAPIView):
    serializer_class = serializers.UserAcceptPublicSerializers
    permission_classes = [permissions.IsAuthenticated, OwnProfilePermission]
    http_method_names = ['get', 'put']
    queryset = Profile.objects.all()

    def get_object(self):
        queryset = self.get_queryset().filter(user_id=self.kwargs['user_id'])
        obj = get_object_or_404(queryset, user_id=self.kwargs['user_id'])
        return obj


class SettingDetailView(generics.RetrieveUpdateAPIView):
    serializer_class = serializers.SettingSerializers
    permission_classes = [permissions.IsAdminUser]
    http_method_names = ['get', 'put']
    queryset = Setting.objects.all()

    def get_object(self):
        obj = get_object_or_404(self.get_queryset(), parameter=self.kwargs['param'])
        return obj


class SettingView(generics.ListCreateAPIView):
    serializer_class = serializers.SettingSerializers
    permission_classes = [permissions.IsAuthenticated]
    queryset = Setting.objects.all()
    http_method_names = ['get']

    def get(self, request, *args, **kwargs):
        if 'param' in kwargs:
            instance = get_object_or_404(self.queryset, parameter=kwargs['param'])
            serializer = self.get_serializer(instance)
            return Response(serializer.data)

        return super().get(request, *args, **kwargs)


class FileUploadAjaxView(generics.CreateAPIView, generics.RetrieveUpdateDestroyAPIView):
    serializer_class = serializers.FileSerializers
    permission_classes = [permissions.IsAuthenticated]
    queryset = File.objects.all()

    def get_object(self):
        obj = get_object_or_404(self.get_queryset(), user_id=self.request.GET.get('user_id'))
        return obj

    def get(self, request, *args, **kwargs):
        if self.get_queryset().filter(user_id=request.GET.get('user_id')).exists():
            serializer = self.get_serializer(
                self.get_queryset().filter(user_id=request.GET.get('user_id')).order_by('-modified').first()
            )
            return Response(serializer.data)

        return Response({})

    def perform_create(self, serializer):
        user = User.objects.get(pk=self.request.GET.get('user_id'))
        serializer.save(user=user)
