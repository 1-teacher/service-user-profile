from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns

from apiV1 import views

user_list = views.UserViewSet.as_view({'get': 'list'})
user_detail = views.UserViewSetDetail.as_view({'get': 'retrieve'})
bill_user = views.BillView.as_view()
bill_detail = views.BillIsPaidView.as_view()
accept_on_public = views.UserAcceptPublicView.as_view()
change_staff = views.UserChangeStaffView.as_view()
setting_view = views.SettingView.as_view()
setting_detail_view = views.SettingDetailView.as_view()
related_files = views.FileUploadAjaxView.as_view()

urlpatterns = format_suffix_patterns([
    # path('test/', views.TestViewSet.as_view({'get': 'get'})),
    path('users/', user_list, name='users'),
    path('users/<int:pk>/', user_detail, name='user_detail'),
    path('users/<int:user_id>/bills/', bill_user, name='bill_user'),
    path('users/<int:user_id>/bills/<int:pk>/', bill_detail, name='bill_detail'),
    path('bills/<int:pk>/', bill_detail, name='bill_detail2'),
    path('accept-on-public/<int:user_id>/', accept_on_public, name='accept_on_public'),
    path('users/<int:user_id>/change_staff/', change_staff, name='change_staff'),
    path('settings/', setting_view, name='setting_view'),
    path('settings/<str:param>/', setting_detail_view, name='setting_detail_view'),
    path('related_files/', related_files, name='related_files')
], allowed=['json'])
