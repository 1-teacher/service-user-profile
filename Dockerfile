FROM python:3.7

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Create app directory
RUN mkdir /app
COPY . /app

WORKDIR /app

RUN python -m pip install \
  --trusted-host pypi.org \
  --trusted-host files.pythonhosted.org \
  -r requirements.txt

#RUN pip install certifi



# Used in wait-for-postgres.sh to connect to PostgreSQL
# Needs to be updated, repeats in docker-compose
ENV POSTGRES_USER postgres
ENV POSTGRES_PASSWORD postgres
ENV POSTGRES_DB postgres

EXPOSE 8000 5432 6379

ENTRYPOINT ["python", "manage.py"]
CMD ["runserver", "0.0.0.0:8000"]