from django.db.models.signals import post_save
from django.dispatch import receiver
from django.conf import settings

from public.models import Profile, Bill


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


from public.tasks import send_email_bill_task


@receiver(post_save, sender=Bill)
def send_email_bill(sender, instance, **kwargs):
    send_email_bill_task.delay({
        'bill_code': instance.bill_code,
        'price': instance.price,
        'domain': settings.MYSITE,
        'is_paid': "Оплачено" if instance.is_paid else "Требуется оплатить",
        'modified': instance.modified,
        'email': instance.user.email
    })
