from django.shortcuts import render, redirect
from django.views import View

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.urls import reverse_lazy
from django.views.generic import FormView

from django.contrib import messages
from django.shortcuts import get_object_or_404
from django.http import HttpResponse

from public.models import Profile, Club
from public.forms import UserProfileForm
import xlwt
import datetime


def get_user_name(user_id):
    user = Profile.objects.filter(user_id=user_id).values('first_name', 'last_name', 'user__email').first()
    user_name = "%s %s %s" % (user["first_name"], user["last_name"], user["user__email"])
    return user_name


class AccountView(View):
    template_name = 'index.html'

    @method_decorator(login_required(login_url=reverse_lazy('sign_in')))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        # if request.user.is_staff:
        #     self.template_name = 'profile_staff.html'

        return render(request, self.template_name, {
            'user_id': request.user.id, 'user_name': get_user_name(request.user.id), 'comment': 'It is you!',
            'is_my_page': True
        })


def is_staff(func):
    def check_perms(request, *args, **kwargs):
        if not request.user.is_staff:
            return render(request, '403.html', status=403)
        return func(request, *args, **kwargs)

    return check_perms


def is_admin_or_owner(func):
    def check_perms(request, *args, **kwargs):
        if not request.user.is_superuser or not request.user.is_staff:
            return render(request, '403.html', status=403)
        return func(request, *args, **kwargs)

    return check_perms


@login_required(login_url=reverse_lazy('sign_in'))
@is_staff
def account_user_for_admin(request, user_id):
    if request.user.id == user_id and (request.user.is_staff or request.user.is_superuser):
        return redirect(reverse_lazy('account'))

    return render(request, 'index.html',
                  {'user_id': user_id, 'user_name': get_user_name(user_id), 'comment': 'other user',
                   'is_my_page': False})


class FormProfileView(FormView):
    form_class = UserProfileForm
    template_name = 'form_profile.html'
    success_url = reverse_lazy('edit_profile')

    @method_decorator(login_required(login_url=reverse_lazy('sign_in')))
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    # @method_decorator(is_admin_or_owner)
    def get(self, request, *args, **kwargs):
        user_id = kwargs['user_id'] if 'user_id' in kwargs and kwargs['user_id'] else request.user.id

        p = get_object_or_404(Profile, user_id=user_id)
        form = UserProfileForm(instance=p)

        return render(request, self.template_name, {
            'form': form, 'user_id': user_id, 'user_name': get_user_name(user_id),
            'is_my_page': True if 'user_id' not in kwargs else False
        })

    # @method_decorator(is_admin_or_owner)
    def post(self, request, *args, **kwargs):
        user_id = kwargs['user_id'] if 'user_id' in kwargs and kwargs['user_id'] else request.user.id

        p = get_object_or_404(Profile, user_id=user_id)
        if 'region_code' in request.POST:
            p.region_code_id = request.POST.get('region_code_id')

        # import re
        # data['phone_work'] = "7" + re.sub(r'\+7(\d{3})(\d{3})(\d{2})(\d{2})', r'\1\2\3\4', data['phone_work'])
        # data['phone_mobile'] = "7" + re.sub(r'\+7(\d{3})(\d{3})(\d{2})(\d{2})', r'\1\2\3\4', data['phone_mobile'])

        form = UserProfileForm(data=request.POST, instance=p)
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, 'Данные обновлены')
        else:
            messages.add_message(request, messages.ERROR, 'Данные не обновлены, ошибки при заполеннии')

        return render(request, self.template_name, {
            'form': form, 'user_id': user_id, 'user_name': get_user_name(user_id),
            'is_my_page': True if 'user_id' not in kwargs else False
        })

    def form_valid(self, form):

        return super().form_valid(form)


@login_required(login_url=reverse_lazy('sign_in'))
def profiles(request):
    from public.models import SubjectRegion
    return render(request, 'public/profiles.html', {
        "filter_subject": SubjectRegion.objects.values('id', 'name'),
        "filter_clubs": Club.objects.values('id', 'name')}
                  )


@login_required(login_url=reverse_lazy('sign_in'))
@is_staff
def settings(request):
    return render(request, 'public/settings.html', None)


@login_required(login_url=reverse_lazy('sign_in'))
def policy(request):
    return render(request, 'public/policy.html', None)


@login_required(login_url=reverse_lazy('sign_in'))
def requirements_article(request):
    return render(request, 'public/requirements_article.html', None)


@login_required(login_url=reverse_lazy('sign_in'))
@is_staff
def export_xls(request):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename="export_users_data__' + datetime.datetime.now().strftime(
        '%a_%d_%b_%Y__%H_%M_%S') + '.xls"'

    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Участники')

    row_num = 0

    font_style = xlwt.XFStyle()
    font_style.font.bold = True

    columns = [
        'Фамилия',
        'Имя',
        'Отчество',
        'Субъект РФ',
        'Город/район',
        'Место работы/учебы',
        'Должность',
        'Звание',
        'Телефон рабочий',
        'Мобильный телефон',
        'Email',
        'Вконтакте',
        'Instagram',
        'Facebook',
        'Одноклассники',
        'Прикреплен файл',
        'Секция',
        'Номер счета',
        'Счет оплачен(да/нет)',
        'Согласие на публикацию статей в сборнике Конференции',
        'Идентификатор пользователя',
        'Дата регистрации'
    ]

    [ws.write(row_num, col_num, columns[col_num], font_style) for col_num in range(len(columns))]

    font_style = xlwt.XFStyle()
    from public.models import Profile

    for row in Profile.objects.filter(user__is_active=True, user__is_superuser=False).values_list(
            'last_name',
            'first_name',
            'patronymic',
            'region_code__name',
            'city',
            'place_work',
            'employee_position',
            'rank',
            'phone_work',
            'phone_mobile',
            'user__email',
            'link_vk',
            'link_instagram',
            'link_facebook',
            'link_ok',
            'user__file',
            'club__name',
            'user__bill__bill_code',
            'user__bill__is_paid',
            'is_accept_on_public',
            'user_id',
            'created'
    ):
        row_num += 1
        row = list(row)

        for col_num in range(len(row)):
            if col_num == 15 or col_num == 19:
                if row[col_num]:
                    row[col_num] = 'да'
                else:
                    row[col_num] = 'нет'
            elif col_num == 18:
                if row[col_num]:
                    row[col_num] = 'оплачен'
                else:
                    row[col_num] = 'не оплачен'
            elif col_num == 21:
                row[col_num] = row[col_num].strftime("%Y-%m-%d %H:%M")

            ws.write(row_num, col_num, row[col_num], font_style)

    wb.save(response)
    return response
