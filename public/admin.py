from django.contrib import admin

from custom_auth.models import User
from public.models import Club

admin.site.register(User)
admin.site.register(Club)

