from django.forms import ModelForm
from public.models import Profile, SubjectRegion, Club, File
from django import forms


class RegionModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.name


class ClubModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return obj.name


class UserProfileForm(ModelForm):
    region_code = RegionModelChoiceField(queryset=SubjectRegion.objects.all(), required=True)

    patronymic = forms.CharField(max_length=50)
    first_name = forms.CharField(max_length=50)
    last_name = forms.CharField(max_length=50)

    city = forms.CharField(max_length=100)
    place_work = forms.CharField(max_length=100)
    employee_position = forms.CharField(max_length=100)

    club = ClubModelChoiceField(queryset=Club.objects.all(), required=True)

    rank = forms.CharField(max_length=100, required=False)
    link_vk = forms.CharField(max_length=100, required=False)
    link_instagram = forms.CharField(max_length=100, required=False)
    link_facebook = forms.CharField(max_length=100, required=False)
    link_ok = forms.CharField(max_length=100, required=False)

    def __init__(self, *args, **kwargs):
        super(UserProfileForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control'

    class Meta:
        model = Profile
        fields = [
            'first_name', 'last_name', 'patronymic', 'city', 'place_work',
            'employee_position', 'phone_work', 'phone_mobile', 'rank', 'club',
            'link_vk', 'link_instagram', 'link_facebook', 'link_ok',
            'region_code'
        ]
