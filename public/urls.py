from django.urls import path
from public.views import AccountView, export_xls, account_user_for_admin, FormProfileView, profiles, settings, policy, \
    requirements_article

urlpatterns = [
    path('', AccountView.as_view(), name='account'),
    path('edit_profile/', FormProfileView.as_view(), name='edit_profile'),
    path('edit_profile/<int:user_id>/', FormProfileView.as_view(), name='edit_profile_user'),
    path('profiles/', profiles, name='profiles_page'),
    path('profiles/<int:user_id>/', account_user_for_admin, name='account_detail'),
    path('settings/', settings, name='settings'),
    path('policy/', policy, name='policy'),
    path('requirements-article/', requirements_article, name='requirements_article'),
    path('export/', export_xls, name='export_users_xls'),
]
