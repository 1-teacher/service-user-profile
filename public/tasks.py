from __future__ import absolute_import

import logging

from django.template.loader import render_to_string
from django.core.mail import EmailMessage
from django.core.mail import EmailMultiAlternatives

from main.celery import app

log = logging.getLogger(__name__)


@app.task(autoretry_for=(Exception,), retry_backoff=True)
def send_activate_email_task(data):
    try:
        mail_subject = 'Активация аккаунта'
        message = render_to_string('acc_active_email.html', data)

        email = EmailMessage(
            mail_subject, message, to=[data['to_email']]
        )
        email.send()
    except Exception as e:
        log.error(e)


@app.task(autoretry_for=(Exception,), retry_backoff=True)
def send_restore_password_email_task(subject, body, from_email, to_email, html_email):
    try:
        email_message = EmailMultiAlternatives(subject, body, from_email, [to_email])
        if html_email:
            email_message.attach_alternative(html_email, 'text/html')

        email_message.send()
    except Exception as e:
        log.error(e)


@app.task(autoretry_for=(Exception,), retry_backoff=True)
def send_email_bill_task(data):
    mail_subject = 'Выставлен счет'
    try:
        message = render_to_string('email/acc_bill_email.html', data)
        email = EmailMessage(
            mail_subject, message, to=[data['email']]
        )
        email.content_subtype = "html"
        email.send()
    except Exception as e:
        log.error(e)
