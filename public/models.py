from django.db import models

from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser

from django.utils import timezone

from django.conf import settings

from uuid import uuid4
import os


class SubjectRegion(models.Model):
    name = models.CharField(max_length=100)
    okato = models.CharField(max_length=15)
    region_code = models.IntegerField(default=0)

    def __str__(self):
        return self.name


class Club(models.Model):
    name = models.CharField(max_length=850, default='')

    def __str__(self):
        return self.name


class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    patronymic = models.CharField(max_length=150, default='')
    first_name = models.CharField(max_length=150, default='')
    last_name = models.CharField(max_length=150, default='')
    region_code = models.ForeignKey(SubjectRegion, on_delete=models.SET_NULL, blank=True, null=True)

    city = models.CharField(max_length=300, default='')
    place_work = models.CharField(max_length=300, default='')
    employee_position = models.CharField(max_length=300, default='')

    phone_work = models.CharField(max_length=20, default='')
    phone_mobile = models.CharField(max_length=20, default='')

    rank = models.CharField(max_length=300, default='')
    club = models.ForeignKey(Club, on_delete=models.SET_NULL, blank=True, null=True)

    link_vk = models.CharField(max_length=300, default='')
    link_instagram = models.CharField(max_length=300, default='')
    link_facebook = models.CharField(max_length=300, default='')
    link_ok = models.CharField(max_length=300, default='')
    is_accept_on_public = models.BooleanField(default=False)

    created = models.DateTimeField(editable=False, default=timezone.now)
    modified = models.DateTimeField(auto_now=True)


def path_and_rename(instance, filename):
    def wrapper(instance, filename):
        ext = filename.split('.')[-1]
        if instance.user.id:
            filename = '{}.{}'.format(instance.user.id, ext)
        else:
            filename = '{}.{}'.format(uuid4().hex, ext)
        return os.path.join('survey_images/', filename)

    return wrapper(instance, filename)


class File(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    src = models.FileField(upload_to=path_and_rename)

    created = models.DateTimeField(editable=False, default=timezone.now)
    modified = models.DateTimeField(auto_now=True)


class Bill(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    bill_code = models.CharField(max_length=20)
    price = models.IntegerField(default=0)
    is_paid = models.BooleanField(default=False)

    created = models.DateTimeField(editable=False, default=timezone.now)
    modified = models.DateTimeField(auto_now=True)


class Setting(models.Model):
    parameter = models.CharField(unique=True, max_length=20)
    is_active = models.BooleanField(default=False)
